from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib.auth import authenticate, login, logout
from .forms import LoginForm
def logout_view(request):
    logout(request)
    return HttpResponseRedirect('/login')
def login_view(request):
    if request.user.is_authenticated():
        return HttpResponseRedirect('/')

    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            u = form.cleaned_data['username']
            p = form.cleaned_data['password']
            user = authenticate(username=u, password=p)
            if user is not None:
                if user.is_active:
                    login(request, user)
                    return HttpResponseRedirect('/')
                else:
                    print('This account disable ')
                    print(user, u, p)
                    return HttpResponseRedirect('/login')
            else:
                print('This username and passwor incorrect ')
                print(user, u, p)
                return HttpResponseRedirect('/login')

    else:
        form = LoginForm()
        return render(request, 'login.html', {'form': form})
