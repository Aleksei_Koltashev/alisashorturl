from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class ShortUrls(models.Model):
    user_id = models.ForeignKey(
                User,
                 on_delete=models.CASCADE,)
    url = models.CharField(max_length = 1000)
    countClick = models.IntegerField(default=0)
    def __str__(self):
        return self.user_id.username + ' ' + self.url + ' ' + str(self.countClick)
