from django.conf.urls import url, include
from UrlCuts import views

urlpatterns = [
    url(r'^$', views.home, name='home'),
    url(r'^cut', views.cut_view, name='cut'),
    url(r'^re(?P<id_url>[A-Za-z0-9_-]+)', views.redirect_view, name='redirect'),



]
