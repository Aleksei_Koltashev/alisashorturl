# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-21 13:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('UrlCuts', '0004_remove_shorturls_shorturl'),
    ]

    operations = [
        migrations.AlterField(
            model_name='shorturls',
            name='url',
            field=models.CharField(max_length=1000),
        ),
    ]
