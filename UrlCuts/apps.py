from django.apps import AppConfig


class UrlcutsConfig(AppConfig):
    name = 'UrlCuts'
