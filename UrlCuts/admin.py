from django.contrib import admin

from .models import ShortUrls
class ShortURLAdmin(admin.ModelAdmin):
    list_filter = ['user_id']
# Register your models here.
admin.site.register(ShortUrls, ShortURLAdmin)


