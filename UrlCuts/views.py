from django.shortcuts import render,  get_object_or_404
from django.http import HttpResponse, HttpResponseRedirect
from .forms import UrlForm
from .models import ShortUrls
import urllib.parse
import re
# Create your views here.
def home(request):
    if not request.user.is_authenticated():
        return HttpResponseRedirect('/login')
    else:
        form = UrlForm()
        return render(request, 'main.html',{'form': form, 'is_auth': True})

def cut_view(request):
    if request.method == 'POST':
        form = UrlForm(request.POST)
        if form.is_valid():
            url = form.cleaned_data['urlinput']
            url =  url.encode("utf8")
            url = urllib.parse.unquote_plus(url.decode("utf-8"),  encoding='utf-8', errors='replace')
            pattern = '^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w\?\s\-&=]*)*\/?$'
            if re.match(pattern, url) is not None:
                shurl = ShortUrls.objects.create(user_id=request.user, url=url)
                sep = ('Полученный url: http://', str(request.get_host()),'/re', str(shurl.id))
                answer = ''.join(sep)
                return render(request, 'main.html',
                              {'data': answer,
                               'form': form,
                              'is_auth': True})
            else:
                return render(request, 'main.html', {'data': url + ' Неправильный URL. URL должен иметь вид HTTP(S)://example.com/sub/...',
                                                    'form': form,
                                                    'is_auth': True})

        else:
            print('invalid')
            return render(request, 'main.html', {
                                       'data': 'Неправильный URL. URL должен иметь вид HTTP(S)://example.com/sub/...',
                                       'form': form,
                                        'is_auth': True})
    else:
        return HttpResponseRedirect('/')

def redirect_view(request, id_url):
    url = get_object_or_404(ShortUrls, id=id_url)
    if not 'is_visited' + str(id_url) in request.COOKIES:
        url.countClick += 1
    url.save()
    response = HttpResponseRedirect(url.url)
    response.set_cookie('is_visited' + str(id_url))
    return response

